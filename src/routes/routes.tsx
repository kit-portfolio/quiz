// MODULES
import React from 'react';
import { Routes , Route } from 'react-router-dom';

// COMPONENTS
import { Home } from 'src/components/home/home';
import { Quiz } from 'src/components/quiz/quiz';
import { Congratulations } from 'src/components/congratulations/congratulations';

// HELPERS
import { page } from 'src/constants/pages.constants';

/**
 * React Router routes controller
 * @returns ReactNode
 */
export function AppRoutes() {
  return (
    <Routes>
      <Route path={page.home} element={<Home />} />
      <Route path={page.question} element={<Quiz />} />
      <Route path={page.congratulations} element={<Congratulations />} />
    </Routes>
  );
}
