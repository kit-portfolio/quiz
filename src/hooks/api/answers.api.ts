// MODULES
import { useCallback, useState } from 'react';
import axios from 'axios';

// RESOURCES
import { route } from 'src/constants/api.routes';
import { IQuestion } from 'src/types/questions.types';
import { IAnswer } from 'src/types/answer.types';

export function usePostAnswer() {
    const [isLoading, setLoading] = useState(false);
    const [answer, setAnswer] = useState<IQuestion[]>([]);

    const postAnswer = useCallback((answer: IAnswer) => {
        setLoading(true);

        return axios
            .post(route.answers, { ...answer })
            .then(data => setAnswer(data.data))
            .catch(e => console.log(e))
            .finally(() => setLoading(false))
    }, []);

    return { postAnswer, isLoading, answer }
}