// MODULES
import { useCallback, useState } from 'react';
import axios from 'axios';

// RESOURCES
import { route } from 'src/constants/api.routes';
import { IQuestion } from 'src/types/questions.types';

export function useFetchQuestion() {
    const [isLoading, setLoading] = useState(false);
    const [question, setQuestion] = useState<IQuestion>();

    const fetchQuestion = useCallback((questionId: string) => {
        setLoading(true);

        axios
            .get(route.question(questionId))
            .then(data => setQuestion(data.data))
            .catch(e => console.log(e))
            .finally(() => setLoading(false))
    }, []);

    return { fetchQuestion, isLoading, question };
}

export function useFetchQuestionsCatalog() {
    const [isLoading, setLoading] = useState(false);
    const [questionList, setQuestionList] = useState<IQuestion[]>([]);

    const fetchQuestionCatalog = useCallback(() => {
        setLoading(true);

        axios
            .get(route.questions)
            .then(data => setQuestionList(data.data))
            .catch(e => console.log(e))
            .finally(() => setLoading(false))
    }, []);

    return { fetchQuestionCatalog, isLoading, questionList }
}