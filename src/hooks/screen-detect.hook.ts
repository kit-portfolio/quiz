// MODULES
import { useCallback, useEffect, useState } from 'react';

// STYLES
import { breakpoint } from 'src/styles/layout.style';

export function useScreenDetect() {
    const initialState = {
        isLayoutSmall: false,
        isLayoutLarge: false,
        isLayoutOversized: false,
    };
    const [screenType, setScreenType] = useState(initialState);

    const detectScreen = useCallback(() => {
        setScreenType({
            isLayoutSmall:
                window.matchMedia(
                    `(max-width: ${breakpoint.l}px)`
                ).matches,
            isLayoutLarge:
                window.matchMedia(
                    `(min-width: ${breakpoint.l}px)`
                ).matches,
            isLayoutOversized:
                window.matchMedia(
                    `(min-width: ${breakpoint.max}px)`
                ).matches,
        });
    }, []);

    useEffect(() => {
        detectScreen();
        window.addEventListener('resize', detectScreen);

        return () => {
            window.removeEventListener('resize', detectScreen);
        };
    }, []);

    return screenType;
}
