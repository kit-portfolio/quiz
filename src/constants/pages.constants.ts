export const page = {
    home: '/',
    question: '/question/:questionId',
    congratulations: '/congratulations'
}