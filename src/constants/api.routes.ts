export const apiBaseUrl = 'http://localhost:8000';

export const route = {
    question: (questionId: string) => `${apiBaseUrl}/api/questions/${questionId}`,
    questions: `${apiBaseUrl}/api/questions`,
    answer: (answerId: string) => `${apiBaseUrl}/api/answers/${answerId}`,
    answers: `${apiBaseUrl}/api/answers`,
}