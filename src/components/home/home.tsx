// MODULES
import React, { useCallback, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { generatePath } from 'react-router';

// RESOURCES
import { useScreenDetect } from 'src/hooks/screen-detect.hook';
import { useFetchQuestionsCatalog } from 'src/hooks/api/questions.api';
import { questionsSlice } from 'src/store/questions/questions.slice';
import { sessionSlice } from 'src/store/session/session.slice';
import { page } from 'src/constants/pages.constants';
import { currentQuestionSlice } from 'src/store/current-question/current-question.slice';
import { Button } from '../misc/button/button';

// STYLES
import { Section, Illustration } from './home.style';

const Home = () => {
    const { isLayoutSmall } = useScreenDetect();
    const { fetchQuestionCatalog, questionList } = useFetchQuestionsCatalog();
    const dispatch = useDispatch();
    const navigate = useNavigate();

    useEffect(() => {
        fetchQuestionCatalog();
    }, []);

    useEffect(() => {
        dispatch(questionsSlice.actions.set(questionList));
        dispatch(currentQuestionSlice.actions.set(0))
    }, [questionList])

    const startQuiz = useCallback(() => {
        dispatch(sessionSlice.actions.start());
        navigate(generatePath(page.question, { questionId: questionList[0].id }))
    }, [questionList])

    return (
        <Section>
            <div>
                <Illustration src="images/home-illustration.png" alt="Homepage illustration" />
            </div>
            <div>
                <h1>Get back in shape.</h1>
                <Button
                    title='Start The Quiz'
                    stretch={isLayoutSmall}
                    onClick={() => startQuiz()}
                />
            </div>
        </Section>
    )
};

export { Home };
