import styled from 'styled-components';
import { breakpoint, layout, spacing } from 'src/styles/layout.style';

export const Section = styled.section`
  display: flex;
  flex-direction: column;
  max-width: ${breakpoint.max}px;
  gap: ${spacing.s};
  justify-content: center;
  height: 100vh;
  margin: 0 ${spacing.s};
  align-self: center;
  
  div {
    text-align: center;
  }
  
  @media ${layout.large} {
    flex-direction: row-reverse;
    align-items: center;

    div {
      text-align: initial;
    }
  }
`;

export const Illustration = styled.img`
  width: 100%;
  height: auto;
`;

