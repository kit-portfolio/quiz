import styled from 'styled-components';

export const CongratulationsStyle = styled.main`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  height: 100vh;
`;

