// MODULES
import React, { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { useDispatch } from 'react-redux';

// RESOURCES
import { sessionSlice } from 'src/store/session/session.slice';
import { page } from 'src/constants/pages.constants';

// STYLES
import { CongratulationsStyle } from './congratulations.style';

const Congratulations = () => {
    const [counter, setCounter] = useState(3);
    const navigate = useNavigate();
    const dispatch = useDispatch();

    useEffect(() => {
        if(!counter) {
            dispatch(sessionSlice.actions.stop)
            navigate(page.home);
        }
        setTimeout(() => setCounter(counter - 1), 1000)
    }, [counter])

    return (
        <CongratulationsStyle>
            <h1>Congratulations!</h1>
            <p>You will be redirected to homepage in {counter} seconds</p>
        </CongratulationsStyle>
    )
};

export { Congratulations };
