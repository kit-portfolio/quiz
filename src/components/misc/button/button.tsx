import React from 'react';
import { Button as ButtonStyle } from './button.style';

export interface IButtonProps {
    title: string;
    stretch?:  boolean;
    isDisabled?:  boolean;
    onClick: () => void;
}

const Button = ({ title, onClick, isDisabled = false, stretch = false }: IButtonProps) => (
    <ButtonStyle disabled={isDisabled} stretch={stretch} onClick={onClick}>
        {title}
    </ButtonStyle>

);

export { Button };
