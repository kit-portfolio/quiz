import styled from 'styled-components';
import { palette } from 'src/styles/palette.style';
import { spacing } from 'src/styles/layout.style';

interface IButtonProps {
    stretch?: boolean;
}

export const Button = styled.button<IButtonProps>`
  color: ${palette.white};
  background-color: ${palette.purple};
  padding: ${spacing.s} ${spacing.l};
  width: ${(props) => (props.stretch ? '100%' : 'auto')};
  border-radius: 8px;
  box-shadow: 0 16px 32px 0 #AA00FF3D;
  text-transform: capitalize;
  border: none;
  max-width: 350px;
  transition: 0.5s;
  
  &:disabled {
    transition: 0.5s;
    background-color: ${palette.lightGray};
    color: ${palette.black};  
    box-shadow: none;
  }
`;