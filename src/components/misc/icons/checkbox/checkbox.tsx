import React from 'react';
import { ArrowSVGStyled } from './checkbox.style';

export interface ICheckboxIconProps {
    isChecked: boolean;
}

const CheckboxIcon = ({ isChecked }: ICheckboxIconProps) => <ArrowSVGStyled isChecked={isChecked} />;

export { CheckboxIcon };
