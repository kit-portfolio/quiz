import styled from 'styled-components';
import { CheckboxSVG } from 'src/resources';
import { palette } from 'src/styles/palette.style';

interface IArrowSVGStyledProps {
    isChecked: boolean;
}

export const ArrowSVGStyled = styled(CheckboxSVG)<IArrowSVGStyledProps>`
    fill: ${(props) => (props.isChecked ? palette.purple : palette.gray)};
`;