import React from 'react';
import { ChevronSVG } from 'src/resources';

const ChevronIcon = () => <ChevronSVG />;

export { ChevronIcon };
