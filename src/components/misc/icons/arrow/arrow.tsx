import React from 'react';
import { ArrowSVGStyled } from './arrow.style';

const ArrowIcon = () => <ArrowSVGStyled />;

export { ArrowIcon };
