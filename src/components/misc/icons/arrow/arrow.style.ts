import styled from 'styled-components';
import { ArrowSVG } from 'src/resources';
import { palette } from 'src/styles/palette.style';

export const ArrowSVGStyled = styled(ArrowSVG)`
    fill: ${palette.gray};
`;