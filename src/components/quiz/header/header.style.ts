import styled from 'styled-components';
import { breakpoint, spacing } from 'src/styles/layout.style';
import { palette } from 'src/styles/palette.style';

export const BackLink = styled.button`
  display: flex;
  gap: ${spacing.s};
  align-items: center;
  border: none;
  background: transparent;
  cursor: pointer;
  padding: ${spacing.s};
`;

export const HeaderContainer = styled.nav`
  position: relative;
`;

interface IHeaderProgressBarStyleProps {
    current: number;
    finish: number;
}

export const HeaderProgressBarStyle = styled.div<IHeaderProgressBarStyleProps>`
  position: absolute;
  bottom: 0;
  height: 2px;
  background-color: ${palette.gray};
  width: 100%;
  
  display: flex;
  
  div {
    transition: 0.5s;
    background-color: ${palette.purple};
    flex-basis: ${({ current, finish }) => `${Math.floor(current/finish*100)}%`};  
  }
`;

export const HeaderContent = styled.nav`
  display: flex;
  justify-content: space-between;
  align-items: center;
  
  padding: ${spacing.s};
  max-width: ${breakpoint.max}px;
  margin: 0 auto;
`;