// MODULES
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';

// RESOURCES
import { selectAllQuestions } from 'src/store/questions/questions.selector';
import { selectCurrentQuestion } from 'src/store/current-question/current-question.selector';
import { ChevronIcon } from 'src/components/misc/icons/chevron/chevron';
import { page } from 'src/constants/pages.constants';
import { currentQuestionSlice } from 'src/store/current-question/current-question.slice';

// STYLES
import { HeaderContent, HeaderContainer, BackLink, HeaderProgressBarStyle } from './header.style';

const Header = () => {
    const questions = useSelector(selectAllQuestions);
    const currentQuestion = useSelector(selectCurrentQuestion);
    const navigate = useNavigate();
    const dispatch = useDispatch();

    function handleBackClick() {
        if (currentQuestion === 0) {
            navigate(page.home);
            return;
        }

        dispatch(currentQuestionSlice.actions.decrement())
        navigate(-1)
    }

    return (
        <HeaderContainer>
            <HeaderContent>
                <BackLink onClick={handleBackClick}>
                    <ChevronIcon />
                    <strong>Back</strong>
                </BackLink>
                <div>{currentQuestion + 1} of {questions.length}</div>
            </HeaderContent>
            <HeaderProgressBarStyle current={currentQuestion + 1} finish={questions.length}>
                <div />
            </HeaderProgressBarStyle>
        </HeaderContainer>
    )
};

export { Header };
