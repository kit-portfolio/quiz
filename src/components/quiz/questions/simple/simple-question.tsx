// MODULES
import React, { useCallback, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import { generatePath } from 'react-router';
import { useDispatch, useSelector } from 'react-redux';

// RESOURCES
import { usePostAnswer } from 'src/hooks/api/answers.api';
import { selectCurrentSession } from 'src/store/session/session.selector';
import { IQuestion } from 'src/types/questions.types';
import { currentQuestionSlice } from 'src/store/current-question/current-question.slice';
import { page } from 'src/constants/pages.constants';
import { selectCurrentQuestion } from 'src/store/current-question/current-question.selector';
import { selectAllQuestions } from 'src/store/questions/questions.selector';
import { ChoiceOption } from '../../options/choice/choice-option';

// STYLES
import { QuestionContainer, OptionsContainer } from '../../quiz.style';

interface ISimpleQuestionProps {
    question: IQuestion;
}

const SimpleQuestion = ({ question }: ISimpleQuestionProps) => {
    const dispatch = useDispatch();
    const { postAnswer } = usePostAnswer();
    const sessionId = useSelector(selectCurrentSession);
    const currentQuestion = useSelector(selectCurrentQuestion);
    const questionList = useSelector(selectAllQuestions);
    const navigate = useNavigate();

    function submitAnswer(variant: string) {
        postAnswer({
            sessionId,
            questionId: question.id,
            createdAt: Date.now(),
            answers: [variant],
        }).then(() => {
                dispatch(currentQuestionSlice.actions.increment())
            }
        )
    }

    useEffect(() => {
        if (currentQuestion === questionList.length) {
            navigate(page.congratulations);
        } else {
            navigate(generatePath(page.question, { questionId: questionList[currentQuestion].id }))
        }
    }, [currentQuestion]);

    const renderOptions = useCallback(() => question.variants.map(item => (
<ChoiceOption
            key={item.label}
            content={item.label}
            onClick={() => submitAnswer(item.value)}
/>
)), [question]);

    return (
        <QuestionContainer>
            <h2>{question.label}</h2>
            {question.description && <p>{question.description}</p>}
            <OptionsContainer>
                {renderOptions()}
            </OptionsContainer>
        </QuestionContainer>
    )
};

export { SimpleQuestion };
