// MODULES
import React, { useCallback, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { generatePath } from 'react-router';

// RESOURCES
import { usePostAnswer } from 'src/hooks/api/answers.api';
import { selectCurrentSession } from 'src/store/session/session.selector';
import { IQuestion, IQuestionVariant } from 'src/types/questions.types';
import { Button } from 'src/components/misc/button/button';
import { currentQuestionSlice } from 'src/store/current-question/current-question.slice';
import { page } from 'src/constants/pages.constants';
import { selectCurrentQuestion } from 'src/store/current-question/current-question.selector';
import { selectAllQuestions } from 'src/store/questions/questions.selector';
import { CheckboxOption } from '../../options/checkbox/checkbox-option';

// STYLES
import { QuestionContainer, OptionsContainer, ButtonContainer } from '../../quiz.style';

interface IMultiselectQuestionProps {
    question: IQuestion;
}

const MultiselectQuestion = ({ question }: IMultiselectQuestionProps) => {
    const dispatch = useDispatch();
    const [answers, setAnswers] = useState<Map<string, IQuestionVariant>>(new Map());
    const { postAnswer } = usePostAnswer();
    const sessionId = useSelector(selectCurrentSession);
    const currentQuestion = useSelector(selectCurrentQuestion);
    const questionList = useSelector(selectAllQuestions);
    const navigate = useNavigate();

    useEffect(() => {
        if (currentQuestion === questionList.length) {
            navigate(page.congratulations);
        } else {
            navigate(generatePath(page.question, { questionId: questionList[currentQuestion].id }))
        }
    }, [currentQuestion]);

    function submitAnswer() {
        const payload = [...answers].map(item => item[1].value)
        postAnswer({
            sessionId,
            questionId: question.id,
            createdAt: Date.now(),
            answers: payload,
        }).then(() => {
                dispatch(currentQuestionSlice.actions.increment())
            }
        )
    }

    const renderOptions = useCallback(() => question.variants.map(item => (
        <CheckboxOption
            key={item.label}
            content={item.label}
            onClick={() => handleClick(item)}
            isChecked={answers.has(item.label)}
        />
    )), [question, answers]);

    function handleClick(target: IQuestionVariant) {
        // Remove answer on second click
        if (answers.has(target.label)) {
            answers.delete(target.label)
            setAnswers(new Map(answers))
        }

        // remove all answers if reset option is chosen
        else if (target.options?.deselectOthers) {
            setAnswers(new Map([[target.label, target]]));

        }

        // Add answer
        else {
            // TODO: deselect deselector if regular option is chosen
            setAnswers(new Map(answers.set(target.label, target)));
        }
    }

    // TODO: align button horizontally
    return (
        <QuestionContainer>
            <h2>{question.label}</h2>
            {question.description && <p>{question.description}</p>}
            <OptionsContainer>
                {renderOptions()}
            </OptionsContainer>
            <ButtonContainer>
                <Button isDisabled={!(answers.size > 0)} title='Continue' onClick={() => submitAnswer()} />
            </ButtonContainer>
        </QuestionContainer>
    )
};

export { MultiselectQuestion };
