// MODULES
import React, { useEffect } from 'react';
import { useParams } from 'react-router';

// RESOURCES
import { useFetchQuestion } from 'src/hooks/api/questions.api';
import { Header } from './header/header';
import { SimpleQuestion } from './questions/simple/simple-question';
import { MultiselectQuestion } from './questions/multiselect/multiselect-question';

// STYLES
import { QuizContainer } from './quiz.style';

const Quiz = () => {
    const { questionId } = useParams();
    const { fetchQuestion, question } = useFetchQuestion();

    useEffect(() => {
        fetchQuestion(questionId!);
    }, [questionId]);

    return (
        <>
            <Header />
            <QuizContainer>
                {question?.type === 'single' && <SimpleQuestion question={question} />}
                {question?.type === 'multiple' && <MultiselectQuestion question={question} />}
            </QuizContainer>
        </>
    )
};

export { Quiz };
