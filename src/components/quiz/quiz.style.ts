import styled from 'styled-components';
import { layout, spacing } from 'src/styles/layout.style';

export const QuizContainer = styled.main`
  flex-grow: 1;
  display: flex;
  justify-content: center;

  @media ${layout.large} {
    align-items: center;
  }
`;

export const QuestionContainer = styled.div`
  max-width: 750px;
  margin: ${spacing.m};
  display: flex;
  flex-direction: column;
  gap: ${spacing.m};
  
  h2 {
    text-align: center;
  }
  
  p {
    text-align: center;  
  }
`;

export const OptionsContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  gap: ${spacing.s};
`;

export const ButtonContainer = styled.div`
  text-align: center;
  
  button {
    width: 350px;
  }
`;
