// MODULES
import styled from 'styled-components';

// RESOURCES
import { palette } from 'src/styles/palette.style';

// STYLES
import { OptionStyle } from '../option.style';

export const ChoiceOptionStyle = styled(OptionStyle)`
  transition: 0.5s;
  box-sizing: border-box;
  
  &:hover {
    
    svg {
      transition: 0.5s;
      fill: ${palette.purple};
    }
  }
`;
