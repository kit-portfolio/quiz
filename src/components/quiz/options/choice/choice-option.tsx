// MODULES
import React, { ReactNode } from 'react';

// RESOURCES
import { ArrowIcon } from 'src/components/misc/icons/arrow/arrow';

// STYLES
import { ChoiceOptionStyle } from './choice-option.style';
import { OptionContentStyle } from '../option.style';

export interface IChoiceOptionProps {
    prefix?: ReactNode;
    content: ReactNode | string;
    onClick: () => void;
}

const ChoiceOption = ({ prefix, content, onClick }: IChoiceOptionProps) => (
    <ChoiceOptionStyle onClick={onClick}>
        {!!prefix && prefix}
        <OptionContentStyle>
            {content}
        </OptionContentStyle>
        <ArrowIcon />
    </ChoiceOptionStyle>
);

export { ChoiceOption };
