// MODULES
import React, { ReactNode } from 'react';

// RESOURCES
import { CheckboxIcon } from 'src/components/misc/icons/checkbox/checkbox';
import { CheckboxOptionStyle } from './checkbox-option.style';

// STYLES
import { OptionContentStyle } from '../option.style';

export interface IChoiceOptionProps {
    prefix?: ReactNode;
    isChecked: boolean;
    content: ReactNode | string;
    onClick: () => void;
}

const CheckboxOption = ({ prefix, content, onClick, isChecked }: IChoiceOptionProps) => (
        <CheckboxOptionStyle  isChecked={isChecked} onClick={onClick}>
            {!!prefix && prefix}
            <OptionContentStyle>
                {content}
            </OptionContentStyle>
            <CheckboxIcon isChecked={isChecked} />
        </CheckboxOptionStyle>
    );

export { CheckboxOption };
