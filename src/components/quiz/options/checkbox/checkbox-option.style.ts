// MODULES
import styled from 'styled-components';

// RESOURCES
import { palette } from 'src/styles/palette.style';

// STYLES
import { OptionStyle } from '../option.style';

interface ICheckboxOptionStyleProps {
    isChecked: boolean;
}

export const CheckboxOptionStyle = styled(OptionStyle)<ICheckboxOptionStyleProps>`
  border: ${(props) => (props.isChecked ? `1px solid ${palette.purple}` : '')};
  box-sizing: border-box;
`;
