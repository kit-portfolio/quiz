// MODULES
import styled from 'styled-components';

// RESOURCES
import { palette } from 'src/styles/palette.style';
import { layout, spacing } from 'src/styles/layout.style';

export const OptionStyle = styled.div`
  display: flex;
  padding: ${spacing.s};
  gap: ${spacing.s};
  align-items: center;
  justify-content: space-between;
  border: 1px solid ${palette.gray};
  border-radius: 8px;
  width: 100%;
  transition: 0.5s;

  &:hover {
    border: 1px solid ${palette.purple};
    transition: 0.5s;
  }
  
  @media ${layout.large} {
    width: 350px;
  }
`;

export const OptionContentStyle = styled.div`
  flex-grow: 1;
`;