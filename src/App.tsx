// MODULES
import React from 'react';
import { Provider } from 'react-redux';
import {
    BrowserRouter as Router,
} from 'react-router-dom';

// RESOURCES
import { store } from './store/store';
import { AppRoutes } from './routes/routes';

const App = () => (
    <Router>
        <Provider store={store}>
            <AppRoutes />
        </Provider>
    </Router>
);

export default App;
