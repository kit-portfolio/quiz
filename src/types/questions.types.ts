export type QuestionsTypesT = 'single' | 'multiple' | 'info';

export interface IQuestionVariantOptions {
    icon?: string;
    iconSize?: number;
    deselectOthers?: boolean;
}

export interface IQuestionVariant {
    label: string;
    value: string;
    options?: IQuestionVariantOptions;
}

export interface IQuestion {
    id: string;
    type: QuestionsTypesT;
    label: string;
    description?: string;
    variants: IQuestionVariant[];
}