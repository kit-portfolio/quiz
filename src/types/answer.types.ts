export interface IAnswer {
    id?: string;
    sessionId: string;
    questionId: string;
    answers: string[];
    createdAt: number;
}