export const fontSize = {
    xl: 'font-size: 64px;',
    l: 'font-size: 40px;',
    m: 'font-size: 28px;',
    s: 'font-size: 16px;',
}