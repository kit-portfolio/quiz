export const breakpoint = {
  max: 1200,
  l: 375,
};

export const spacing = {
  l: '32px',
  m: '24px',
  s: '12px',
}

export const layout = {
  large: `(min-width: ${breakpoint.l}px)`,
};
