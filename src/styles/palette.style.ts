export const palette = {
    black: '#212121',
    gray: '#ededed',
    lightGray: '#eeeeee',
    purple: '#aa00ff',
    white: 'white',
}