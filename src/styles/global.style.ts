import { createGlobalStyle } from 'styled-components';
import { fontSize } from './typography.style';
import { palette } from './palette.style';
import { layout } from './layout.style';

export const GlobalStyle = createGlobalStyle`
  #root {
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    user-select: none;
    
    min-height: 100vh;
  }
  
  h1 {
    color: ${palette.black};
    ${fontSize.m}
    
    @media ${layout.large} {
      ${fontSize.xl}
    }
    
  }
`