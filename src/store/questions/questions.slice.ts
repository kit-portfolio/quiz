import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import { IQuestion } from 'src/types/questions.types';

export interface IQuestionsState {
    list: IQuestion[]
}

const initialState: IQuestionsState = {
    list: [],
}

export const questionsSlice = createSlice({
    name: 'questions',
    initialState,
    reducers: {
        set: (state, action: PayloadAction<IQuestion[]>) => {
            state.list = action.payload;
        },
        clear: (state) => {
            state.list = [];
        },
    },
})

export const { set, clear } = questionsSlice.actions

export default questionsSlice.reducer