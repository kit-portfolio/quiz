import { createSelector } from '@reduxjs/toolkit';
import { RootState } from '../store';

export const questionSelect = (state: RootState) => state.questions.list;

export const selectAllQuestions = createSelector(
    questionSelect,
    (state) => state
);

export const selectQuestion = createSelector(
    questionSelect,
    (state) => state
);
