import { createSlice, PayloadAction } from '@reduxjs/toolkit'

export interface ICurrentQuestionState {
    value: number
}

const initialState: ICurrentQuestionState = {
    value: 0,
}

export const currentQuestionSlice = createSlice({
    name: 'current question index',
    initialState,
    reducers: {
        increment: (state) => {
            state.value++
        },
        decrement: (state) => {
            state.value--
        },
        set: (state, action: PayloadAction<number>) => {
            state.value = action.payload
        },
    },
})

export const { increment, decrement, set } = currentQuestionSlice.actions

export default currentQuestionSlice.reducer