import { createSelector } from '@reduxjs/toolkit';
import { RootState } from '../store';

export const currentQuestionSelect = (state: RootState) => state.currentQuestion.value;

export const selectCurrentQuestion = createSelector(
    currentQuestionSelect,
    (state) => state
);
