import { createSelector } from '@reduxjs/toolkit';
import { RootState } from '../store';

export const sessionSelect = (state: RootState) => state.session;

export const selectCurrentSession = createSelector(
    sessionSelect,
    (state) => state.id
);
