import { v4 as uuid } from 'uuid';
import { createSlice, PayloadAction } from '@reduxjs/toolkit'

export interface ISessionState {
    id: string
}

const initialState: ISessionState = {
    id: '',
}

export const sessionSlice = createSlice({
    name: 'session',
    initialState,
    reducers: {
        start: (state) => {
            state.id = uuid();
        },
        stop: (state, action: PayloadAction<number>) => {
            state.id = '';
        },
    },
})

export const { start, stop } = sessionSlice.actions

export default sessionSlice.reducer