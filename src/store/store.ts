// MODULES
import { configureStore } from '@reduxjs/toolkit';

// REDUCERS
import { currentQuestionSlice } from './current-question/current-question.slice';
import { questionsSlice } from './questions/questions.slice';
import { sessionSlice } from './session/session.slice';

export const store = configureStore({
  reducer: {
    currentQuestion: currentQuestionSlice.reducer,
    questions: questionsSlice.reducer,
    session: sessionSlice.reducer,
  },
});

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;