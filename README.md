### Launching project
To launch project locally follow the procedure:

1. Launch the mock server
```shell
    yarn server:mock:start
```
2. Launch application
```shell
    yarn app:start
```

Your app will be hosted on [localhost:3000](http://localhost:3000/)

